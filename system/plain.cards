@. Plain TeXnicard (standard edition) -- version 0.1
@. This file is licensed under the same terms as TeXnicard itself.

@. As a special exception, if you create a document which uses this file,
@. and link it with this file, this file does not by itself cause the
@. resulting document to be covered by the GNU General Public License.
@. This exception does not however invalidate any other reasons why the
@. document might be covered by the GNU General Public License. If you
@. modify this file, you may extend this exception to your version of the
@. file, but you are not obligated to do so. If you do not wish to do so,
@. delete this exception statement from your version.

@0
\font\logo=logo10 \def\MF{{\logo META}\-{\logo FONT}}
\font\biglogo=logo10 scaled\magstep2 \def\bigMF{{\biglogo METAFONT}}
\advance\hfuzz by 4pt

This is Plain \TeX nicard, a standard set of macros and definitions which
can be used with \TeX nicard. In most cases you will use this file with
another template for the game which you are making cards of. (Which means,
this file is called a ``meta-template''.)

Subroutines defined here are described using a stack effect notation,
similar to the stack notation commonly used in Forth.
@0

@. --- Utilities ---

@0
Here are some utility codes which are useful to use with \TeX nicard, just
some miscellaneous stuff that doesn't belong elsewhere and should be
defined before the other things are defined.

The first thing is the \.{Finally} command, which is just a convenient way
to add things which should be done at the end of the processing, after the
metatemplate, template, and set, have all been loaded and processed.

@E
[]sZ

@S Finally
@. ( subroutine -- )
B[x]+lZr+sZ

@0
This subroutine is used to compose functions. Just using \.+ to do this
will not always work, because they might be numbers and the premature
exiting and required spaces between numbers can easily cause that not to
work.

@S Compose
@. ( code code -- code )
rB[x]+rB[x]++

@0
Here is a simple code to determine whether a value on the stack is a
number or a string.

@S Data_Type
@. ( value -- type )
BArD91-0 1di

@0
Compute power of ten. (It is used in the next code after this one.)

@S Pow10
@. ( exponent -- result )
1r[d1d0iQ1-r10*rY]xD

@0
This is used to make a number displayed with decimal places. The input
will be integer, but it is displayed as a fractional number with decimal
places, such as ``\.{325.04}'' when the number is in units of $1/100$ you
can make real display units.

This is used when doing statistics calculations, such as average, standard
deviation, variance, etc.

@S Decimals
@. ( number places -- string )
<(Pow10)xs2dl2%l2+BAD[.]r+rl2/Br+>

@. --- Table operations ---

@0
Here we are defining some subroutines to deal with tables. Please note
that the \.{table-number} parameter can be written like \.{`S} to refer
to the \.S table, for example.

The \.{Mark\_Table} command is used for marking a value in many positions
of a table all at once. The list of indices is given as a string. Example:
\.{[aeiou]1`v(Mark\_Table)x}

@S Mark_Table
@. ( string-of-indices value table-number -- )
<[:]ra+s2s1[Al1rl2xY]xD>

@0
The \.{Copy\_Table} command copies a table; it is sometimes very useful.

@S Copy_Table
@. ( src-table-number dst-table-number -- )
<[:]ra+s2[;]ra+s10[ddl1xrl2x1+d256-0 1diQY]xD>

@0
The \.{Fill\_Table} command fills the entire table with one value. This
might be used to reset a table to zero, for instance.

@S Fill_Table
@. ( value table-number -- )
<[:]ra+s2s10[dl1rl2x1+d256-0 1diQY]xD>

@0
Here is a command to create a table that maps every number to itself. The
resulting table might then be modified.

@S Identity_Table
@. ( table-number -- )
<[:]ra+s2256[1-ddl2xd1d0iQY]xD>

@. --- String operations ---

@0
Here are subroutines for a few common string operations.

@S Split_String
@. ( string char -- left right )
<[]s1s2[Adl2-0 1 0iQal1r+s1Y]xDl1r>

@S Reverse_String
@. ( string -- string )
<[]s1[Aal1+s1Y]xDl1>

@S Cut_Beginning
@. ( string amount -- string )
[1-d0 1 0iQrADrY]xD

@0
The \.{Translate} command is used to change characters in a string
according to a table. It can also be used to convert a string into
uppercase, or into lowercase.

@S Translate
@. ( string table-number -- string )
<[]s1[;]ra+s2[Al2xal1r+s1Y]xDl1>

@S Sentence_Case
@. ( string -- string )
A;Uar+

@0
The other string operation is to convert into the format expected by Plain
\TeX, for making reports and so on. Some characters such as \.\{ and \.\}
will not be used, but \.\&, \.\$, \.\#, and \.\% are a bit common. A new
table \.e is used to select which ones.

@E
0`e(Fill_Table)x
[&$%#]1`e(Mark_Table)x

@S To_Plain_TeX
@. ( string -- string )
<[]s1[Ad;e[]d[\]il1r+s1al1r+s1Y]xDl1>

@. --- Natural sorting ---

@0
You can read the description of the natural sorting algorithm in the
\TeX nicard program book. It requires the \.S table to be filled in, in
order to work. That is what this chapter of this file is for.

@S Sort_Letters
@. ( string-of-characters -- )
<128s1[Al1r:Sl11+s1Y]xD>

@S Sort_Priority
@. ( string-of-characters -- )
<[Ad;S64+r:SY]xD>

@S Sort_Digits
@. ( string-of-characters -- )
<64s1[Al1r:Sl11+s1Y]xD>

@0
Most characters are treated like spaces/punctuation (category 2). Field
terminators and so on are treated as the end of the text to match. This
file configures it to use the English alphabet; in your own templates, you
might add accented letters to allow different languages to be used, if you
want to make a card game with French writing or some other language.

@E
2`S(Fill_Table)x
0 4:S 0 9:S 0 30:S 0 31:S
[0123456789](Sort_Digits)x
[ABCDEFGHIJKLMNOPQRSTUVWXYZ](Sort_Letters)x
[abcdefghijklmnopqrstuvwxyz](Sort_Letters)x
[IX](Sort_Priority)x
3`,:S 4`.:S

@. --- Set information ---

@0
The title is stored here at first. It is blank, so that you can just use
\.{@@S Title} to type the new one.

@E Title
[](Title)S

@S Title_To_Plain_TeX
(Title)L(To_Plain_TeX)x

@. --- Plurals ---

@0
Here is patterns for making plurals of English. You can add exceptions in
your template in case the game uses special words. (The rules and word
list is based on Damian Conway's list.)

These plurals are conditionally selected based on whether the name
\.{(English)} is given a nonzero value in the template which includes this
one; this way you can disable them if you do not need English.

Mostly Anglicized plurals are used here due to simpler rules.

@E English
(English)L
@<

@0
But first, the specification of characters in a word form rule.

@E
1`W(Fill_Table)x
[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789]
0`W(Mark_Table)x
2`[:W 3`]:W

@W
#0

@. Normal plural rules "-s" "-es" (level 5 and 10)
5:]:s
10:s]:ses
10:ch]:ches
10:sh]:shes

@. Rules for "-ys" "-ies" (level 10 and 15)
15:ay]:ays
15:ey]:eys
15:iy]:iys
15:oy]:oys
15:uy]:uys
10:y]:ies

@. Rules for "-ves" (level 10 and 15)
10:alf]:alves
10:arf]:arves
15:deaf]:deafs
10:eaf]:eaves
10:elf]:elves
10:life]:lives
10:nife]:nives
10:olf]:olves
10:thief]:thieves
10:wife]:wives

@. Uninflected nouns (level 500)
>Bd500 0
=bison
=carp
=corps
=debris
=djinn
=elk
=flounder
=gallows
=innings
=mackerel
=pants
=pliers
=salmon
=scissors
=sea-bass
=series
=species
=swine
=trout
=tuna
=whiting
=wildebeest

@. Uninflected suffix (level 500)
>BADd500 0
=deer
=fish
=ois
=pox
=sheep

@. Irregular suffix (level 500)
>`:(Split_String)xrBADr500 0
=foot:feet
=goose:geese
=human:humans
=louse:lice
=man:men
=mouse:mice
=tooth:teeth
=xis:xes
=zoon:zoa

@. Irregular plurals (level 1000)
>`:(Split_String)xBrBr1000 0
=ephemeris:ephemerides
=soliloquy:soliloquies
=ox:oxen
=mythos:mythoi

@. "-um" to "-a" (level 750)
>d[um]+Br[a]+750 0
=agend
=bacteri
=candelabr
=dat
=extrem
=strat
=ov

@. "-" to "-im" (level 750)
>d[im]+BrBr750 0
=cherub
=goy
=seraph

@. "-a" to "-ae" (level 750)
>dBr[e]+750 0
=alga
=alumna
=vertebra

@. For a complete set of category tables, please see:
@. http://www.csse.monash.edu.au/~damian/papers/HTML/Plurals_AppendixA.html

@>

@. --- English numbers ---

@0
There is another thing controlled by the \.{(English)} parameter.
Converting numbers to words is also conditionally enabled. These can be
grouped into three kinds: ``1'' to ``one'', ``1'' to ``first'', and ``1''
to ``1st''.

@E English
(English)L
@<

@0
The last case, ``1'' to ``1st'', is simplest, so it comes first.

@S 1_1st
@. ( number -- text )
<[th]s0[st]s1[nd]s2[rd]s3
dBr
100%d10%r10/1-1 0 1i*
d3-1d0i*
`0+L+>

@0
The other two are also simple, if they are done using a word form table.
So, that is what we will do. Word form table 1 will be used for numbers
into words.

@W
#1

2:0]:]
2:1]:one
2:2]:two
2:3]:three
2:4]:four
2:5]:five
2:6]:six
2:7]:seven
2:8]:eight
2:9]:nine
3:10]:ten
3:11]:eleven
3:12]:twelve
3:13]:thirteen
3:14]:fourteen
3:15]:fifteen
3:16]:sixteen
3:17]:seventeen
3:18]:eighteen
3:19]:nineteen
4:2t]:twenty
4:3t]:thirty
4:4t]:forty
4:5t]:fifty
4:6t]:sixty
4:7t]:seventy
4:8t]:eighty
4:9t]:ninety
5:-th]:th
5:ty-th]:tieth
5:one-th]:first
5:two-th]:second
5:three-th]:third
5:five-th]:fifth
5:eight-th]:eighth
5:nine-th]:ninth
5:twelve-th]:twelfth

@0
Now we have the definition for \.{(1\_one)} and \.{(1\_first)}. The second
one is mostly like the first one, so it will call the first one. You
should not input negative numbers to these subroutines, because it will
just assume it is zero.

@S 1_one
@. ( number -- text )
d[D[zero]]d[<s1
l1100%B1WD@. Get words for number 1-19
l1100%20-[](1_one_20to99)dix@. Deal with prefix for 20-90
l1100/10%s2[hundred](1_one_1000)x@. Deal with hundreds
l11000/1000%s2[thousand](1_one_1000)x@. Deal with thousands
l11000000/1000%s2[million](1_one_1000)x@. Deal with millions
l11000000000/s2[billion](1_one_1000)x@. Deal with billions
>]ix

@S 1_one_20to99
AD@. Remove the tens digit left from before
l110/10%B[t]+1WD@. Make word for 20-90
rdZ0[][-]ir+@. Figure out whether a hyphen should be added
+@. Join the tens word with the ones word

@S 1_one_1000
l20[D][
l2(1_one)x@. A recursive call, get word for this number
[ ]+r+@. Add the suffix
rdZ0[][ ]ir+@. Figure out if a space is needed
+@. Join the result
]ix

@S 1_first
@. ( number -- text )
(1_one)x[-th]+1WD

@>

@. --- Card lists ---

@0
We have a list storing pointers to the cards and data used for sorting and
for statistics. One use of this sorting is to automatically number the
cards in the correct order regardless of the order in which they are
entered.

Please note there is maximum 32 fields!

Register \.{(Card\_Field\_Number)} selects which is the number for the
next field. Register \.{(Field\_Command)} stores a subroutine number (or a
subroutine code) so that it will decide what to do with the fields. (This
is similar to the current ``mood'' in BBL/Abundance.)

@E Card_Field_Number
0(Card_Field_Number)S

@E Field_Command
@. >( ?? field-number -- ?? )
[D](Field_Command)S

@S Add_Field
@. ( name -- )
(Card_Field_Number)LB[(Field_Command)Lx]+rS
(Card_Field_Number)dL1+rS

@0
Just before listing the fields to sort (and group) by, it should clear the
current list so that it can be filled in by fresh.

@S Begin_Sort
@. ( -- )
0`G(Fill_Table)x

@0
Now here are some of the moods of card fields. Also the register \.9 is
used as a parameter sometimes.

At first, we have the list of sort orders. There is also a special value
to record the sorted position so that they can be numbered even after
changing sorting to something else. They can also be sorted based on the
previous results in this way.

@S _Sort_Order
Lrl9r:G

@S Primary_Ascending
@. >( -- )
`As9(_Sort_Order)(Field_Command)S

@S Primary_Descending
@. >( -- )
`Zs9(_Sort_Order)(Field_Command)S

@S Secondary_Ascending
@. >( -- )
`as9(_Sort_Order)(Field_Command)S

@S Secondary_Descending
@. >( -- )
`zs9(_Sort_Order)(Field_Command)S

@S Record_Sorted_Position
@. >( -- )
`Rs9(_Sort_Order)(Field_Command)S

@0
Next is the command to write fields. It will be the one usually used while
in a card area, and then you can just use multiple fields to write to
without specifying writing every time.

@S Write_Fields
@. >( value -- )
[Lf](Field_Command)S

@0
We also need reading fields! It can also be used multiple times, like the
one above can be.

@S Read_Fields
@. >( -- value )
[Lv](Field_Command)S

@0
Some words might be placed in names so that their numbers can be placed in
card lists and compared. A conversion table \.n is used to change any
parentheses that might be inside the text into control characters and back
again, since parentheses are not allowed in any register names.

Each such name will begin with an asterisk (which is automatically added
and removed as necessary). This is so that it will not interfere with
existing names for commands and so on.

These registers also have a value, although the present program does not
use it. You might find a use for it in your own templates, though.

@E
`n(Identity_Table)x
`(1:n`)2:n1`(:n2`):n

@S Name_To_Text
@. ( name -- text )
NAD`n(Translate)x

@S Text_To_Name
@. ( text -- name )
`n(Translate)x[)]+[(*]r+x

@. --- Statistics ---

@0
There are two field moods for doing statistics with fields. One for
setting grouping, and one for selecting the statistics field.

The current group by field is saved in register \.9.

@S Group_By
@. >( -- )
[s9](Field_Command)S

@0
Now it might be useful to compute the statistics. Statistics includes:
mean, median, minimum, maximum, and variance.

Variance is the mean of the square minus the square of the mean. In
mathematical notation: $$\sigma^2=\left({s_2\over s_0}\right)-\left(
{s_1\over s_0}\right)^2$$

In each group, the code will be executed, storing statistics as follows:
count in \.0, sum in \.1, sum of squares in \.2, minimum in \.3, maximum
in \.4, median in \.5, mean in \.6, variance in \.7, and stat decimals
multiplier in \.8.

You need to set the number of decimal places in statistics, first.

@E Stat_Decimals
2(Stat_Decimals)S

@S _Statistics_Set
(Stat_Decimals)L(Pow10)xs8
s4l8*2/s5s3s2s1s0
l8l1*l0/s6

@0
Now, in order to calculate the variance, we will rearrange the equation
above in order to increase accuracy and allow any number of decimal places
in the result. The rearranged equation is as follows:
$$\sigma^2={s_2s_0-s_1^2\over s_0^2}$$

Now all multiplications are done first before division, in order to
minimize rounding errors. However, this equation still does not deal with
decimal places. Here is the final equation including decimal places (where
$d$ is the number of decimal places, and $r$ is the integer result which
we are looking for):
$$r=\left\lfloor{10^d(s_2s_0-s_1^2)\over s_0^2}\right\rfloor$$

Sorting is done here, for single-dimensional statistics. (Two-dimensional
statistics comes later.)

@S _Statistics_Set
l2l0*l1d*-l8*l0d*/s7

@S Do_Statistics
@. >( code -- )
(Begin_Sort)x`Al9:G
[<d`al9:Gr(_Statistics_Set)r(Compose)xrl9g>](Field_Command)S

@0
Text statistics are printed out using Plain \TeX. The resulting \.{DVI}
file should be processed normally, rather than with \TeX nicard. If any
special fonts or parameters are needed, they should be specified in the
template code.

The first thing is to set up the date and time for the statistics report.

@F statistics.tex
\newcount\hours
\newcount\minutes
\hours=\time
\divide\hours by 60
\minutes=\hours
\multiply\minutes by -60
\advance\minutes by \time
\def\lznum#1{\ifnum#1<10 0\fi\number#1}@. Print leading zero
\def\date{\hbox{%@. Do not break the line within the date/time
  \the\year/\lznum\month/\lznum/day
  \quad \lznum\hours:\lznum\minutes
}}

@0
At end end of the file, \z{bye} will be added. This finishes a {\TeX}
input file.

@E
[(statistics.tex)dL10a[\bye]++rS](Finally)x

@0
Some of the parameters will be changed, such as having no paragraph
indentation and so on. French spacing is selected to ensure the names are
typed as names rather than as sentences.

Also, the footer line will now show the date, time, and set title, in
addition to the page number.

@F statistics.tex
\parindent=0pt
\pretolerance=10000
\frenchspacing
\parskip=1pt

\def\title{@^C(Title_To_Plain_TeX)}
\def\clap#1{\hbox to 0pt{\hss#1\hss}}
\footline={\tenrm\rlap\title\hss\clap\folio\hss\llap\date}

@0
There are {\TeX} macros for making the tables with the data.

@F statistics.tex
\def\statpage#1#2{
  \tabskip=4pt plus 1fil
  \halign to\hsize{##\hss&&\hss$##$\cr
    \omit\hss\bf#1\hss\span\relax\cr
    #2%
  }\filbreak
}

@. --- End of file ---

@0
That's all folks!

@E
