% memusage.w -- Program to check amount of memory used by other programs.

% This program is public domain, but if you combine it with GPL program,
% the combination is licensed by GPL.

\def\title{MEMUSAGE}
\emergencystretch=\hsize

@s FILE int
@s size_t int

@*Introduction. This program is used to trace usage of dynamic memory of
another program. It is not usable with all programs; it is meant to be
used with \TeX nicard, although you might modify it to work with other
programs too.

@c
@<Include files@>@;
@<Global variables@>@;
@<Procedure codes@>;
@<Macros to override standard functions@>@;
#include "memusage_other_program.c"

@ Some include files are used here since they are used in the rest of the
program.

@<Include files@>=
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

@*Initialization. A log file is opened so that the information about
dynamic memory can be logged. Also, it is kept track of how much memory is
being used in total. Another thing which must be kept track of is the list
of allocated pointers so that when |realloc| and |free| is used, it knows
what memory is becoming unused.

@<Global variables@>=
FILE*memusage_fp;
size_t memusage_total=0;
void**memusage_list;
size_t*memusage_sizes;

@ @<Procedure codes@>=
int main(int argc,char**argv) {
  memusage_fp=fopen("memusage.log","w");
@.memusage.log@>
  @<Initialize |memusage_list| and |memusage_sizes|@>;
  fprintf(memusage_fp,"Time %u: Program started\n",(unsigned int)clock());
  return your_main(argc,argv);
}

@ The function |your_main| is the main function of the other program. It
will be a macro here to override it.

@<Macros to override standard functions@>=
#define main @!your_main

@ Null pointers in |memusage_list| represent unused fields, while the
pointer to |&memusage_list| itself is used as the terminator. There is
also |memusage_sizes| which are sizes of data in |memusage_list|.

@<Initialize |memusage_list| and |memusage_sizes|@>= {
  int i;
  memusage_list=malloc(16*sizeof(void*));
  memusage_sizes=malloc(16*sizeof(size_t));
  for(i=0;i<16;i++) memusage_list[i]=memusage_sizes[i]=0;
  memusage_list[15]=&memusage_list;
}

@*Usage Tracking. Memory usage is tracked by a function called
|memusage_track| which looks in |memusage_list| and also does logging of
the information.

@<Procedure codes@>=
void memusage_track(void*ptr,void*newptr,size_t size,char*message) {
  int i; // Where is |ptr| found?
  if(!ptr && !newptr) @<Log the empty pointer and |return|@>;
  @<Check where |ptr| is found in the list@>;
  if(memusage_list[i]==&memusage_list)
    @<Allocate more memory for |memusage_list| and |memusage_sizes|@>;
  @<Update the memory usage arrays and total@>;
  @<Log the memory usage and message to the file@>;
}

@ @<Log the empty pointer and |return|@>= {
  fprintf(memusage_fp,"NULL [%08X]  -- %s\n",(int)size,message);
  return;
}

@ @<Check where |ptr| is found in the list@>= {
  for(i=0;memusage_list[i]!=&memusage_list;i++) {
    if(memusage_list[i]==ptr) break;
  }
}

@ @<Allocate more memory for |memusage_list| and |memusage_sizes|@>= {
  int j;
  memusage_list=realloc(memusage_list,i*sizeof(void*)*2);
  memusage_sizes=realloc(memusage_sizes,i*sizeof(size_t)*2);
  for(j=i;j<i*2;j++) memusage_list[j]=memusage_sizes[j]=0;
  memusage_list[j-1]=&memusage_list;
}

@ @<Update the memory usage arrays and total@>= {
  memusage_total+=size-memusage_sizes[i];
  memusage_list[i]=newptr;
  memusage_sizes[i]=size;
}

@ @<Log the memory usage and message to the file@>= {
  fprintf(memusage_fp,"[%08X]  %08X  %08X  [%08X]  -- %s\n",
    i,(int)ptr,(int)newptr,(int)size,message);
  fprintf(memusage_fp,"Time %u: Total %08X\n",(unsigned int)clock(),
    memusage_total);
}

@ There is one more thing, so that the program can make up its own log
messages.

@d memusage_log(_text,_arg1)
  fprintf(memusage_fp,"Time %u: %s [%08X]\n",(unsigned int)clock(),
    _text,_arg1);

@*Overrides. Now to override standard functions so that they can be
tracked. Most of these are very simple.

The first one is |malloc|.

@<Procedure codes@>=
void*my_malloc(size_t size) {
  void*p=malloc(size);
  memusage_track(0,p,size,"malloc");
  return p;
}

@ Now |free|.

@<Procedure codes@>=
void my_free(void*ptr) {
  free(ptr);
  memusage_track(ptr,0,0,"free");
}

@ Next comes |realloc|.

@<Procedure codes@>=
void*my_realloc(void*ptr,size_t size) {
  void*p=realloc(ptr,size);
  memusage_track(ptr,p,size,"realloc");
  return p;
}

@ Now comes |strdup|, which is a bit more complicated than the others, but
not by much.

@<Procedure codes@>=
char*my_strdup(const char*s1) {
  char*p=strdup(s1);
  memusage_track(0,p,strlen(s1)+1,"strdup");
  return p;
}

@ Now let's override them by macros so that the other programs will call
our functions instead, and that they will be traced.

@<Macros to override standard functions@>=
#define malloc my_malloc
#define free my_free
#define realloc my_realloc
#define strdup my_strdup

@*Index.
